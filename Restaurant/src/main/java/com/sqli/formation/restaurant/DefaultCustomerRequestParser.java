package com.sqli.formation.restaurant;

public class DefaultCustomerRequestParser implements CustomerRequestParser {

	private String customerOrder;
	private String customerName;

	public void parse(String customerRequest) {
		customerName = customerRequest.split(":")[0].trim();
		customerOrder = customerRequest.split(":")[1].trim();
	}

	public String getOrderName() {
		return customerOrder;
	}

	public Customer createCustomer() {
		return new Customer(customerName);
	}

}
