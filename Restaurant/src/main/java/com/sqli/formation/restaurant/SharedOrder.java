package com.sqli.formation.restaurant;

import java.util.List;

public class SharedOrder implements OrderStrategy {
	private int nbOfCustomerForDish;
	Table table;

	public SharedOrder(Table table, int nbOfCustomerForDish) {
		super();
		this.nbOfCustomerForDish = nbOfCustomerForDish;
		this.table = table;
	}

	public void execute(String orderName, int indexOfCustomer,
			List<Customer> customers) {
		int nb = 1;
		for (Customer customer : customers) {
			if (customer.getOrder().equals(orderName))
				nb++;
		}
		customers.get(indexOfCustomer).setOrder(orderName);
		int remainingCustomerForDish = nbOfCustomerForDish-nb;
		if (remainingCustomerForDish>0)
			table.setWarning("MISSING " + remainingCustomerForDish + " for "
					+ orderName);

	}
}
