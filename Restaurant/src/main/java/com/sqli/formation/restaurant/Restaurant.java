package com.sqli.formation.restaurant;

import java.util.HashMap;
import java.util.Map;

public class Restaurant {
	Map<Integer, Table> tables;

	public Restaurant() {
		tables = new HashMap<Integer, Table>();
	}

	public int initTable(int nbOfCustumers) {
		int tableID = generateTableID();
		tables.put(tableID, new Table(nbOfCustumers));
		return tableID;
	}

	public void customerSays(int tableID, String customerRequest) {
		Table table = tables.get(tableID);
		table.customerSays(customerRequest);
	}

	public String createOrder(int tableId) {
		
		Table table = tables.get(tableId);
		return table.createOrder();
	}

	private int generateTableID() {
		return tables.size() + 1;
	}
}
