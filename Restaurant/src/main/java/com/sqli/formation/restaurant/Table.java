package com.sqli.formation.restaurant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.sqli.formation.framework.Context;

public class Table {
	private ArrayList<Customer> customers;
	private Map<String, OrderStrategy> orders;
	private int nbOfPlaces;
	private String warningMsg = "";
	private CustomerRequestParser customerRequestParser=Context.getInstance("CustomerRequestParser");

	public Table(int nbOfPlaces) {
		System.out.println(customerRequestParser);
		this.nbOfPlaces = nbOfPlaces;
		customers = new ArrayList<Customer>();
		orders = new HashMap<String, OrderStrategy>();
		orders.put("Soup", new SimpleOrder());
		orders.put("Chips", new SimpleOrder());
		orders.put("Spaghetti", new SimpleOrder());
		orders.put("Roastbeef", new SimpleOrder());
		orders.put("Same", new SameOrder());
		orders.put("Fish for 2", new SharedOrder(this, 2));

	}

	public void customerSays(String customerRequest) {
		customerRequestParser.parse(customerRequest);
		String customerOrder = customerRequestParser.getOrderName();
		Customer customer = customerRequestParser.createCustomer();
		if (customers.indexOf(customer) == -1)
			customers.add(customer);

		System.out.println(customer + " => " + customerOrder + " "
				+ orders.get(customerOrder));
		orders.get(customerOrder).execute(customerOrder,
				customers.indexOf(customer), customers);
	}

	public String createOrder() {
		int nbOfCustomers = customers.size();
		int emptyPlaces = nbOfPlaces - nbOfCustomers;
		if (emptyPlaces > 0)
			return "MISSING " + emptyPlaces;

		StringBuilder orderResult = new StringBuilder();
		if (!warningMsg.isEmpty()) {
			orderResult.append(warningMsg);
			clearWarningMsg();
		} else {
			formatOrderResult(orderResult);
		}
		return orderResult.toString();
	}

	private void formatOrderResult(StringBuilder orderResult) {
		for (Customer customer : customers) {
			orderResult.append(customer.getOrder() + ", ");
		}
		orderResult.delete(orderResult.length() - 2, orderResult.length());
	}

	private void clearWarningMsg() {
		warningMsg = "";
	}

	public void setWarning(String warningMsg) {
		this.warningMsg = warningMsg;
	}
}
