package com.sqli.formation.restaurant;

import java.util.List;

public class SameOrder implements OrderStrategy {

	public void execute(String orderName, int indexOfCustomer,
			List<Customer> customers) {
		Customer previousCustomer = customers.get(indexOfCustomer - 1);
		System.out.println(previousCustomer);
		customers.get(indexOfCustomer).setOrder(previousCustomer.getOrder());
	}

}
