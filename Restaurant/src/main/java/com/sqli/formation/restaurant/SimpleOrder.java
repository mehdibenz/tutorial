package com.sqli.formation.restaurant;

import java.util.List;

public class SimpleOrder implements OrderStrategy {

	public void execute(String orderName,int indexOfCustomer, List< Customer> customers) {
		System.out.println("simple "+customers.get(indexOfCustomer));
		customers.get(indexOfCustomer).setOrder(orderName);
	}

}
