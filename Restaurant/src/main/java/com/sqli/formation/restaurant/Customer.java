package com.sqli.formation.restaurant;

public class Customer {
	private String name;
	private String order="";

	public Customer(String customerName) {
		this.name = customerName;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "Customer [customerName=" + name + ", order=" + order
				+ "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		Customer customer=(Customer)obj;
		return name.equals(customer.name);
	}
	
}
