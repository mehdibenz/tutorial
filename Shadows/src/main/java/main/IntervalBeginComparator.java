package main;

import java.util.Comparator;

public class IntervalBeginComparator implements Comparator<Interval> {
	public int compare(Interval interval, Interval otherInterval) {
		if (interval.getBegin() == otherInterval.getBegin())
			return 0;
		return (interval.getBegin() > otherInterval.getBegin() ? 1 : -1);
	}

}
