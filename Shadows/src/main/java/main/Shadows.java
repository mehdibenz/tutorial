package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Shadows {

	ShadowsParser shadowsParser = Context.getInstance("ShadowsParser");
	List<Building> buildings = new ArrayList<Building>();
	private ShadowsPrinter shadowsPrinter = Context
			.getInstance("shadowsPrinter");

	public Shadows(float... values) {
		shadowsParser.parse(values);
		buildings = shadowsParser.getBuildings();
	}

	public String project(int rayon) {
		List<Interval> intervals = new ArrayList<Interval>();
		intervals = calculateShadowsInterval(rayon);
		sortIntervalsWithBeginning(intervals);
		intervals = mergeIntervals(intervals);
		return shadowsPrinter.print(intervals);
	}

	private List<Interval> mergeIntervals(List<Interval> intervals) {
		List<Interval> mergedIntervals = new ArrayList<Interval>();
		mergedIntervals.add(intervals.get(0));
		int mergedIntervalsIndex = 0;
		for (int i = 1; i < intervals.size(); i++) {
			mergedIntervalsIndex = tryToMerge(intervals, mergedIntervals,
					mergedIntervalsIndex, i);
		}
		return mergedIntervals;
	}

	private int tryToMerge(List<Interval> intervals,
			List<Interval> mergedIntervals, int mergedIntervalsIndex, int i) {
		if (intervals.get(i)
				.canMerge(mergedIntervals.get(mergedIntervalsIndex))) {
			Interval mergedInterval = intervals.get(i).merge(
					mergedIntervals.get(i - 1));
			System.out.println("can merge" + mergedInterval);
			mergedIntervals.set(mergedIntervalsIndex, mergedInterval);
		} else {
			mergedIntervals.add(intervals.get(i));
			mergedIntervalsIndex++;
		}
		return mergedIntervalsIndex;
	}

	private List<Interval> calculateShadowsInterval(int rayon) {
		List<Interval> intervals = new ArrayList<Interval>();
		for (Building building : buildings) {
			Interval calculatedInterval = building
					.calculateShadowInterval(rayon);
			System.out.println(calculatedInterval);
			intervals.add(calculatedInterval);
		}
		return intervals;
	}

	private void sortIntervalsWithBeginning(List<Interval> intervals) {
		Collections.sort(intervals, new IntervalBeginComparator());
	}

}
