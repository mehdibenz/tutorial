package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Context {
	private Map<String, Object> objects;


	private static class SingletonHolder {
		
		private static final Context context = new Context();

		public static Context getContext() {
			return context;
		}
		
	}

	private Context() {
		System.out.println("Creating context");
		try {
			objects = new TreeMap<String, Object>(String.CASE_INSENSITIVE_ORDER);
			parseConfig();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	private void parseConfig() throws FileNotFoundException,
			ClassNotFoundException, InstantiationException,
			IllegalAccessException {
		File configFile = new File("config.txt");
		Scanner configScanner = new Scanner(configFile);
		String line;
		while (configScanner.hasNext()) {
			line = configScanner.nextLine();
			String name = line.split(" ")[0];
			String className = line.split(" ")[1];
			instantiateObject(name, className);

		}
	}

	private void instantiateObject(String name, String className)
			throws ClassNotFoundException, InstantiationException,
			IllegalAccessException {
		System.out.println("instantiating object with name=" + name
				+ " className=" + className);

		Class<?> objectClass = Class.forName(className);
		Object object = objectClass.newInstance();
		objects.put(name, object);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getInstance(String name) {
		return (T) SingletonHolder.getContext().objects.get(name);
	}
	

	public static void main(String[] args) {
		Shadows shadows = new Shadows(4f, 0f);
		shadows.project(30);
	}
}
