package main;

public class Building {
	private float position;
	private float height;

	public Building(float length, float position) {
		super();
		this.position = position;
		this.height = length;
	}

	public double calculateShadowLength(int rayon) {
		double shadowLength = height / Math.tan(Math.toRadians(rayon));
		return shadowLength;
	}

	public Interval calculateShadowInterval(int rayon) {
		System.out.println("" + position + " " + height );
		double end = calculateShadowLength(rayon)+position;
		System.out.println("--"+end);
		Interval newInterval = new Interval(position, (float)end);
		return newInterval;
	}

}

