package main;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

public class DefaultShadowsPrinter implements ShadowsPrinter {
	
	public String print(List<Interval> intervals) {
		StringBuilder result = new StringBuilder();
		for (Interval interval : intervals) {
			int begin = (int) interval.getBegin();
			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator('.');
			DecimalFormat df = new DecimalFormat("#0.00");
			df.setDecimalFormatSymbols(dfs);
			String end = df.format(interval.getEnd());

			result.append("[" + begin + "," + end + "]");
		}
		return result.toString();
	}
	
}
