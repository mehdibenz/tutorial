package com.sqli.formation.parking;

public class EnabledBay extends Bay {
	@Override
	public boolean park() {
		isEmpty = false;
		return true;
	}

	@Override
	public boolean isEmpty() {
		return isEmpty;
	}

	@Override
	public boolean parkM() {
		if (!isEmpty)
			return false;
		isEmpty = false;
		return true;
	}

	@Override
	public boolean parkC() {
		if (!isEmpty)
			return false;
		isEmpty = false;
		return true;
	}

}
