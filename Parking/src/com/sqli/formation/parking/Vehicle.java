package com.sqli.formation.parking;

import java.security.KeyStore.Entry;
import java.util.List;
import java.util.Map;

public enum Vehicle {
	C() {
		

		@Override
		boolean parkToBay(List<Bay> bays) {
			// TODO Auto-generated method stub
			return false;
		}
	},
	M() {
		boolean parkToBay(List<Bay> bays) {
			return false;
		}
	},
	D() {
		boolean parkToBay(List<Bay> bays) {
			return false;
		}
	};

	abstract boolean parkToBay(List<Bay> bays);
}
