package com.sqli.formation.parking;

public class DisabledBay extends Bay {


	@Override
	public boolean park() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public boolean parkD() {
		if(!isEmpty)
			return false;
		isEmpty=false;
		return true;
	}

}
