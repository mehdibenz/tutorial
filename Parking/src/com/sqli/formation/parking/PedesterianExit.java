package com.sqli.formation.parking;

public class PedesterianExit extends Bay {


	@Override
	public boolean park() {
		return false;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

}
