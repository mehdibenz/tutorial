package com.sqli.formation.playoff;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import com.sqli.formation.playoff.parser.QualificationResultsParser;
import com.sqli.formation.playoff.parser.SpaceQualificationResultsParser;
import com.sqli.formation.playoff.printer.PlayoffLayoutPrinter;
import com.sqli.formation.playoff.printer.SpacedPlayoffLayoutPrinter;
import com.sqli.formation.playoff.qualifier.Qualifier;
import com.sqli.formation.playoff.qualifier.BestTeamsQualifier;

public class Playoff implements IPlayoff {
	// can change
	TreeSet<Team> teams;
	QualificationResultsParser qualificationResultsParser;
	PlayoffLayoutPrinter playoffLayoutPrinter;
	Qualifier qualifier;

	public Playoff(String... qualificationsResults) {
		teams = new TreeSet<Team>();
		qualificationResultsParser = new SpaceQualificationResultsParser(this);
		playoffLayoutPrinter = new SpacedPlayoffLayoutPrinter();
		qualifier = new BestTeamsQualifier();

		qualificationResultsParser.parse(qualificationsResults);
	}

	public String[] layout() {
		TreeSet<Team> selectedBestTeams = qualifier.getQualifiedTeams(teams);
		List<Team[]> expectedPlayoffMatches = calculateExpectedPlayoffMatches(selectedBestTeams);
		String[] arrayResult = playoffLayoutPrinter
				.print(expectedPlayoffMatches); 
		return arrayResult;
	}

	private List<Team[]> calculateExpectedPlayoffMatches(TreeSet<Team> tempTeams) {
		List<Team[]> expectedPlayoffMatches = new ArrayList<Team[]>();
		while (tempTeams.size() > 1) {
			Team weakestTeam = tempTeams.pollLast();
			Team strongestTeam = tempTeams.pollFirst();
			expectedPlayoffMatches
					.add(new Team[] { strongestTeam, weakestTeam });
		}
		return expectedPlayoffMatches;
	}

	public void setTeamScores(TreeSet<Team> teamScores) {
		this.teams = teamScores;
	}
}
