package com.sqli.formation.playoff.parser;

public interface QualificationResultsParser {
	void parse(String[] qualificationsResults);
}
