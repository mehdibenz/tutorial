package com.sqli.formation.playoff.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import com.sqli.formation.playoff.IPlayoff;
import com.sqli.formation.playoff.Team;

public class SpaceQualificationResultsParser implements
		QualificationResultsParser {
	IPlayoff playoff;
	List<Team> teamScores;

	public SpaceQualificationResultsParser(IPlayoff playoff) {
		super();
		this.playoff = playoff;
	}

	public void parse(String[] qualificationsResults) {
		teamScores = new ArrayList<Team>();
		for (String qualificationResult : qualificationsResults) {
			String winnerTeamName = qualificationResult.split(" ")[0];
			addWinner(winnerTeamName);
		}
		playoff.setTeamScores(new TreeSet<Team>(teamScores));
	}

	private void addWinner(String winnerTeamName) {
		boolean found = false;
		for (Team teamScore : teamScores) {
			if (teamScore.getTeamName().equals(winnerTeamName)) {
				teamScore.win();
				found = true;
				break;
			}
		}
		if (!found)
			teamScores.add(new Team(winnerTeamName, 0));
	}

}
