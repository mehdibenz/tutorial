package com.sqli.formation.parking.bay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.sqli.formation.parking.printer.BayPrinter;

public abstract class Bay implements Comparable<Bay> {
	protected Integer index;
	protected Integer distanceToNearestPedestrianExit;
	protected boolean isEmpty = true;
	protected BayPrinter bayPrinter;

	public Bay() {

	}

	public Bay(int index, List<Integer> pedestrianExits) {
		this();
		this.distanceToNearestPedestrianExit = calculateDistanceToNearestPedestrianExit(
				index, pedestrianExits);
		this.index = index;
	}

	private static int calculateDistanceToNearestPedestrianExit(int index,
			List<Integer> pedestrianExits) {
		if (pedestrianExits.isEmpty()) {
			return -1;
		}
		List<Integer> distances = new ArrayList<>();
		for (Integer pedestrianExit : pedestrianExits) {
			int distance = Math.abs(pedestrianExit - index);
			distances.add(distance);
		}
		return Collections.min(distances);
	}

	public int parkC() {
		return -1;
	};

	public int parkM() {
		return -1;
	};

	public int parkD() {
		return -1;
	};

	public boolean unpark(int index) {
		if (!isEmpty && this.index == index) {
			isEmpty = true;
			return true;
		}
		return false;
	}

	public boolean isEmpty() {
		return isEmpty;
	}

	@Override
	public int compareTo(Bay bay) {
		if (!distanceToNearestPedestrianExit
				.equals(bay.distanceToNearestPedestrianExit)) {
			return this.distanceToNearestPedestrianExit
					.compareTo(bay.distanceToNearestPedestrianExit);
		}
		return index.compareTo(bay.index);
	}

	public String print() {
		return bayPrinter.print(this);
	}

}
