package com.sqli.formation.parking.bay;

import java.util.List;

import com.sqli.formation.parking.printer.PedestrianBayPrinter;

public class PedesterianExit extends Bay {


	public PedesterianExit(int index, List<Integer> pedestrianExits) {
		this.index=index;
		this.distanceToNearestPedestrianExit=0;
		this.isEmpty=false;
		bayPrinter=new PedestrianBayPrinter();
		
	}

	@Override
	public boolean unpark(int index) {
		return false;
	}
	
}
