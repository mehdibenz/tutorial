package com.sqli.formation.parking;

import java.util.TreeSet;

import com.sqli.formation.parking.bay.Bay;

public enum Vehicle {
	C() {

		@Override
		int park(Bay bay) {
			return bay.parkC();
		}

	},
	M() {

		@Override
		int park(Bay bay) {
			return bay.parkM();
		}

	},
	D() {

		@Override
		int park(Bay bay) {
			return bay.parkD();
		}

	};

	abstract int park(Bay bay);

	int parkToBay(TreeSet<Bay> sortedBays) {
		int whereToParkIndex = -1;
		for (Bay bay : sortedBays) {
			whereToParkIndex = this.park(bay);
			if (whereToParkIndex != -1)
				return whereToParkIndex;
		}
		return whereToParkIndex;
	}
}
