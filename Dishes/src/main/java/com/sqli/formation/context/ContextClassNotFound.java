package com.sqli.formation.context;

public class ContextClassNotFound extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContextClassNotFound(String className) {
		super("Class with name = " + className + " not found.");
	}
}
