package com.sqli.formation.dishes.orientation.parser;

import java.util.List;

import com.sqli.formation.dishes.orientation.Orientation;
import com.sqli.formation.dishes.orientation.factory.OrientationFactory;

public interface OrientationParser {
	List<Orientation> parseOrientation(String orientationInput);

	void setOrientationFactory(OrientationFactory orientationFactory);
}
