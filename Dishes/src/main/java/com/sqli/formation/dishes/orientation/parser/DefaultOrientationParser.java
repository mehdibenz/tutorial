package com.sqli.formation.dishes.orientation.parser;

import java.util.ArrayList;
import java.util.List;

import com.sqli.formation.dishes.orientation.Orientation;
import com.sqli.formation.dishes.orientation.OrientationType;
import com.sqli.formation.dishes.orientation.factory.OrientationFactory;

public class DefaultOrientationParser implements OrientationParser {
	private OrientationFactory orientationFactory;

	public List<Orientation> parseOrientation(String orientationInput) {
		List<Orientation> orientations = new ArrayList<Orientation>();
		for (String orientationInputElement : orientationInput.split(",")) {
			Orientation orientation = createOrientationFromInput(orientationInputElement);
			orientations.add(orientation);
		}
		return orientations;
	}

	private Orientation createOrientationFromInput(
			String orientationInputElement) {
		int inputLength = orientationInputElement.length();
		double deviationAngle = Double.parseDouble(orientationInputElement
				.substring(0, inputLength - 1));
		String orientationType = orientationInputElement.substring(
				inputLength - 1, inputLength);
		Orientation orientation = orientationFactory.createOrientation(
				deviationAngle, OrientationType.valueOf(orientationType));
		return orientation;
	}

	public void setOrientationFactory(OrientationFactory orientationFactory) {
		this.orientationFactory = orientationFactory;
	}
}
