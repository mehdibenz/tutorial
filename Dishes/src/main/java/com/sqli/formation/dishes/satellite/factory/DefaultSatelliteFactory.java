package com.sqli.formation.dishes.satellite.factory;

import com.sqli.formation.dishes.orientation.Orientation;
import com.sqli.formation.dishes.orientation.OrientationType;
import com.sqli.formation.dishes.orientation.factory.DefaultOrientationFactory;
import com.sqli.formation.dishes.satellite.Satellite;

public class DefaultSatelliteFactory implements SatelliteFactory {

	public Satellite createSatellite(String reference, double deviationAngle,
			OrientationType orientationType) {
		Orientation orientation = new DefaultOrientationFactory()
				.createOrientation(deviationAngle, orientationType);
		Satellite satellite = new Satellite(orientation, reference);
		return satellite;
	}

}
