package com.sqli.formation.dishes.orientation;


public class EastOrientationState implements OrientationState {

	public void affect(double deviationAngle, Orientation orientationToAffect) {
		orientationToAffect.incrementDeviationAngle(deviationAngle);
	}

	@Override
	public String toString() {
		return "EastOrientationState []";
	}

}
