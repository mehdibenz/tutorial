package com.sqli.formation.dishes.satellite.factory;

import com.sqli.formation.dishes.orientation.OrientationType;
import com.sqli.formation.dishes.satellite.Satellite;

public interface SatelliteFactory {

	Satellite createSatellite(String reference, double deviationAngle, OrientationType orientationType);

}
