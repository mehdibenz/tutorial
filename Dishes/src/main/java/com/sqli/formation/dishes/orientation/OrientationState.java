package com.sqli.formation.dishes.orientation;


public interface OrientationState {

	public void affect(double deviationAngle,Orientation orientation);

}
