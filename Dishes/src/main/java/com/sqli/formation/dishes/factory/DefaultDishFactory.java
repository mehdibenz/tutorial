package com.sqli.formation.dishes.factory;

import com.sqli.formation.dishes.Dish;
import com.sqli.formation.dishes.orientation.Orientation;
import com.sqli.formation.dishes.orientation.OrientationType;
import com.sqli.formation.dishes.orientation.factory.OrientationFactory;

public class DefaultDishFactory implements DishFactory {
	OrientationFactory orientationFactory;

	public Dish createDishWithOrientation(int dishID, double dishDeviation,
			String dishOrientation) {
		Orientation orientation = createOrientation(dishDeviation, dishOrientation);
		Dish dish = createDish(dishID);
		dish.addOrientation(orientation);
		return dish;
	}

	private Orientation createOrientation(double dishDeviation,
			String dishOrientation) {
		return orientationFactory.createOrientation(
				dishDeviation, OrientationType.valueOf(dishOrientation));
	}

	public Dish createDish(int dishID) {
		Dish dish = new Dish(dishID);
		return dish;
	}

	public void setOrientationFactory(OrientationFactory orientationFactory) {
		this.orientationFactory = orientationFactory;
	}

}
