package com.sqli.formation.dishes.orientation;

public class WestOrientationState implements OrientationState {

	public void affect(double deviationAngle, Orientation orientationToAffect) {
		orientationToAffect.decrementDeviationAngle(deviationAngle);
	}

	@Override
	public String toString() {
		return "WestOrientationState []";
	}

}
