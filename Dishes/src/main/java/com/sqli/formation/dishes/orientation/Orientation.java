package com.sqli.formation.dishes.orientation;

import com.sqli.formation.context.DishesConfig;

public class Orientation {
	protected double deviationAngle;
	protected OrientationState orientationState;

	public Orientation(double deviation,
			OrientationState orientationMoveStrategy) {
		super();
		this.deviationAngle = deviation;
		this.orientationState = orientationMoveStrategy;
	}

	public double getDeviationAngle() {
		return deviationAngle;
	}

	public void incrementDeviationAngle(double increment) {
		deviationAngle += increment;
	}

	public void decrementDeviationAngle(double increment) {
		deviationAngle -= increment;
	}

	public int calculateSignalQualityToOrientation(Orientation otherOrientation) {
		double deviationAngleDifference = Math.abs( Math.abs(deviationAngle)
				-  Math.abs(otherOrientation.deviationAngle));
		deviationAngleDifference = standariseDifference(deviationAngleDifference);
		int signalQuality = deviationAngleDifferenceToSignalQuality(deviationAngleDifference);
		return signalQuality < 0 ? 0 : signalQuality;
	}

	private int deviationAngleDifferenceToSignalQuality(
			double deviationAngleDifference) {
		return (int) (DishesConfig.MAX_SIGNAL_QUALITY - Math
				.round(deviationAngleDifference));
	}

	private double standariseDifference(double difference) {
		difference = (difference * DishesConfig.MAX_SIGNAL_QUALITY);
		return difference;
	}

	public void affect(Orientation orientation) {
		orientationState.affect(deviationAngle, orientation);
	}

	public void setOrientationState(OrientationState orientationState) {
		this.orientationState = orientationState;
	}

	@Override
	public String toString() {
		return "Orientation [deviationAngle=" + deviationAngle
				+ ", orientationMoveStrategy=" + orientationState + "]";
	}

}
