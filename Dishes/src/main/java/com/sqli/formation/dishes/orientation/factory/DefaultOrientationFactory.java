package com.sqli.formation.dishes.orientation.factory;

import com.sqli.formation.dishes.orientation.EastOrientationState;
import com.sqli.formation.dishes.orientation.Orientation;
import com.sqli.formation.dishes.orientation.OrientationType;
import com.sqli.formation.dishes.orientation.WestOrientationState;

public class DefaultOrientationFactory implements OrientationFactory {

	public Orientation createOrientation(double deviationAngle,
			OrientationType orientationType) {
		switch (orientationType) {
		case E:
			return new Orientation(deviationAngle, new EastOrientationState());
		case W:
			return new Orientation(deviationAngle, new WestOrientationState());
		}
		throw new IllegalArgumentException();
	}

}
