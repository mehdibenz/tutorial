package com.sqli.formation.dishes.parser;

import java.util.ArrayList;
import java.util.List;

import com.sqli.formation.dishes.Dish;
import com.sqli.formation.dishes.factory.DishFactory;
import com.sqli.formation.dishes.orientation.Orientation;
import com.sqli.formation.dishes.orientation.factory.OrientationFactory;
import com.sqli.formation.dishes.orientation.parser.OrientationParser;

public class DefaultDishesParser implements DishesParser {
	List<Dish> dishes;

	DishFactory dishFactory;
	OrientationFactory orientationFactory;
	OrientationParser orientationParser;

	public void parse(String[] dishesInput) {
		dishes = new ArrayList<Dish>();
		for (String dishInput : dishesInput) {
			Dish dish = createDishWithOrientationsFromInput(dishInput);
			dishes.add(dish);
		}
	}

	private Dish createDishWithOrientationsFromInput(String dishInput) {
		Dish dish = createDishFromInput(dishInput);
		List<Orientation> orientations = createOrientationsFromInput(dishInput);

		addOrientationsToDish(dish, orientations);

		return dish;
	}

	private void addOrientationsToDish(Dish dish, List<Orientation> orientations) {
		for (Orientation orientation : orientations) {
			dish.addOrientation(orientation);
		}
	}

	private List<Orientation> createOrientationsFromInput(String dishInput) {
		String dishOrientationsString = dishInput.substring(
				dishInput.indexOf(",") + 1, dishInput.length());

		List<Orientation> orientations = orientationParser
				.parseOrientation(dishOrientationsString);
		return orientations;
	}

	private Dish createDishFromInput(String dishInput) {
		String dishIDString = dishInput.substring(0, dishInput.indexOf(","));
		int dishID = Integer.parseInt(dishIDString);
		Dish dish = dishFactory.createDish(dishID);
		return dish;
	}

	public void setDishFactory(DishFactory dishFactory) {
		this.dishFactory = dishFactory;
	}

	public void setOrientationFactory(OrientationFactory orientationFactory) {
		this.orientationFactory = orientationFactory;
	}

	public void setOrientationParser(OrientationParser orientationParser) {
		this.orientationParser = orientationParser;
	}

	public List<Dish> getDishes() {
		return dishes;
	}

}
