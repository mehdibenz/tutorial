package com.sqli.formation.dishes.signal.printer;

import com.sqli.formation.context.DishesConfig;

public class DefaultSignalPrinter implements SignalPrinter {
	private static final String SIGNAL_IDENTIFIER = "*";
	private static final String NOSIGNAL_IDENTIFIER = ".";
	private static final String NOSIGNAL_TEXT = "No signal !";
	private String resultPattern = "|%s|";

	public String printSignalWithValue(int signalValue) {
		if (signalValue == 0)
			return NOSIGNAL_TEXT;

		StringBuilder result = new StringBuilder();
		appendSignalQuality(signalValue, result);
		appendDifference(signalValue, result);
		return String.format(resultPattern, result);
	}

	private void appendDifference(int signalValue, StringBuilder result) {
		for (int i = signalValue; i < DishesConfig.MAX_SIGNAL_QUALITY; i++) {
			result.append(NOSIGNAL_IDENTIFIER);
		}
	}

	private void appendSignalQuality(int signalValue, StringBuilder result) {
		for (int i = 0; i < signalValue; i++) {
			result.append(SIGNAL_IDENTIFIER);
		}
	}

}
