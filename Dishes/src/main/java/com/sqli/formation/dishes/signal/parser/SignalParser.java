package com.sqli.formation.dishes.signal.parser;


public interface SignalParser {

	void parse(String signalInput);

	String getSatelliteReference();

}
