package com.sqli.formation.dishes.signal.parser;



public class DefaultSignalParser implements SignalParser{

	private String satelliteReference;

	public void parse(String signalInput) {
		satelliteReference = signalInput.split(",")[1].trim();
	}

	public String getSatelliteReference() {
		return satelliteReference;
	}

}
